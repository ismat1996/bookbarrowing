defmodule BookBarrow.Library.Catalog do
  use Ecto.Schema
  import Ecto.Changeset

  @unique_constraint {:title, :string, autogenerate: false}
  schema "catalogs" do
    field :code, :string
    field :title, :string
    has_one :loans, BookBarrow.Book.Loans, foreign_key: :title
    timestamps()
  end

  @doc false
  def changeset(catalog, attrs) do
    catalog
    |> cast(attrs, [:title, :code])
    |> validate_required([:title, :code])
    |> unique_constraint(:title)
  end
end
