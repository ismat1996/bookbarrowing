defmodule BookBarrow.Book.Loans do
  use Ecto.Schema
  import Ecto.Changeset


  schema "loans" do
    field :code, :string
    field :end_date, :string
    field :start_date, :string
    field :times_extended, :string
    belongs_to :users, BookBarrow.Library.Catalog, foreign_key: :title, references: :catalogs, type: :string

    timestamps()
  end

  @doc false
  def changeset(loans, attrs) do
    loans
    |> cast(attrs, [:title, :code, :start_date, :end_date, :times_extended])
    |> validate_required([:title, :code, :start_date, :end_date, :times_extended])  
    |> foreign_key_constraint(:title)
  end
end
