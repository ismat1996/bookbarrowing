defmodule BookBarrowWeb.PageController do
  use BookBarrowWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
