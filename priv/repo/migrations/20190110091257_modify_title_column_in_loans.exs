defmodule BookBarrow.Repo.Migrations.ModifyTitleColumnInLoans do
  use Ecto.Migration

  def change do
      alter table(:loans) do
        modify :title, references(:catalogs, column: :title, type: :string, on_delete: :nothing)
      end
  end
end
