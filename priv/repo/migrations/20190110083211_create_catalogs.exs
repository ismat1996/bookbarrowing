defmodule BookBarrow.Repo.Migrations.CreateCatalogs do
  use Ecto.Migration

  def change do
    create table(:catalogs) do
      add :title, :string
      add :code, :string

      timestamps()
    end
  end
end
