defmodule BookBarrow.Repo.Migrations.AddIndexToCatalog do
  use Ecto.Migration

  def change do
    create unique_index(:catalogs, [:title], name: :title_uk)
  end
end
